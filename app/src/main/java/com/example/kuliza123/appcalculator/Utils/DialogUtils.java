package com.example.kuliza123.appcalculator.Utils;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.kuliza123.appcalculator.Activities.FinalActivity;
import com.example.kuliza123.appcalculator.Activities.MainActivity;
import com.example.kuliza123.appcalculator.R;

/**
 * Created by kuliza214 on 1/9/15.
 */
public class DialogUtils {
    public static void onBackPressDialog(final MainActivity activity, String message, double width, double height, Boolean backButtonCancelable, Boolean touchOutsideCancelable){
        Context context = (Context)activity;
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.back_press_verification);
        dialog.setCancelable(backButtonCancelable);
        dialog.setCanceledOnTouchOutside(touchOutsideCancelable);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int DialogWidth = (int) (size.x * width);
        int DialogHeight = (int) (size.y * height);

        dialog.getWindow().setLayout(DialogWidth, DialogHeight);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onYes();
                dialog.cancel();
            }
        });
    }
    public static void onHomeIconPressed(final FinalActivity activity, String message, double width, double height, Boolean backButtonCancelable, Boolean touchOutsideCancelable){
        Context context = (Context)activity;
        final Dialog dialog = new Dialog(context);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.back_press_verification);
        dialog.setCancelable(backButtonCancelable);
        dialog.setCanceledOnTouchOutside(touchOutsideCancelable);

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int DialogWidth = (int) (size.x * width);
        int DialogHeight = (int) (size.y * height);

        dialog.getWindow().setLayout(DialogWidth, DialogHeight);
        dialog.show();

        Button btnNo = (Button) dialog.findViewById(R.id.btnNo);
        Button btnYes = (Button) dialog.findViewById(R.id.btnYes);

        btnNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
            }
        });
        btnYes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.onHomeClick();
                dialog.cancel();
            }
        });
    }
}
