package com.example.kuliza123.appcalculator.Adapter;

import android.app.ActionBar;
import android.content.Context;
import android.content.res.AssetManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Views.TextViewRoboto;

import java.util.List;


/**
 * Created by kuliza123 on 29/7/15.
 */
public class GridViewAdapter extends RecyclerView.Adapter<GridViewAdapter.ListHolder> {

    private Context mContext;
    private List<PlatformListModel> mList;
    private Boolean mSingleSelect;
    private UpdatePriceCallback mUpdatePrice;
    private int mViewHeight;
    private View mView;

    public GridViewAdapter(Context context,List<PlatformListModel>  list, Boolean singleSelect, UpdatePriceCallback priceCallback ,int viewHeight){
        mContext=context;
        mList=list;
        mSingleSelect=singleSelect;
        mUpdatePrice=priceCallback;
        mViewHeight = viewHeight;
    }

    @Override
    public ListHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.gridview_items,parent,false);
        view.setMinimumHeight(mViewHeight);
        ListHolder mListHolder = new ListHolder(view);
        return mListHolder;
    }

    @Override
    public void onBindViewHolder(final ListHolder mListHolder, final int position) {
        if(position < mList.size()){
        mListHolder.mListName.setText(mList.get(position).getName().toString());
        if(mList.get(position).getIsSelected()){
            mListHolder.mListImage.setImageResource(mList.get(position).getSelectedImage());
            mListHolder.mListName.setTextColor(mContext.getResources().getColor(R.color.completed_text_color));
            //mListHolder.mRLGridView.setBackgroundColor(mContext.getResources().getColor(R.color.selected_grid_item));
            if(position%2 == 0){
                mListHolder.mRLGridView.setBackgroundResource(R.drawable.selected_three_sides_border);
            }
            else {
                mListHolder.mRLGridView.setBackgroundResource(R.drawable.selected_grid_item);
            }
        }
        else
        {
            mListHolder.mListName.setTextColor(mContext.getResources().getColor(R.color.white));
            mListHolder.mListImage.setImageResource(mList.get(position).getImage());
            //mListHolder.mRLGridView.setBackgroundColor(mContext.getResources().getColor(R.color.unselected_grid_item));
            if(position%2 == 0){
                mListHolder.mRLGridView.setBackgroundResource(R.drawable.unselected_three_sides_border);
            }
            else {
                mListHolder.mRLGridView.setBackgroundResource(R.drawable.unselected_grid_item);
            }
        }
        if(mSingleSelect){
            mListHolder.mRLGridView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v){
                    if(mList.get(position).getIsSelected()){
                        unSelectOthers();
                        mList.get(position).setIsSelected(false);
                        notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                    else{
                        unSelectOthers();
                        mList.get(position).setIsSelected(true);
                        notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                }
            });
        }
        if(mSingleSelect == false) {
            mListHolder.mRLGridView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!mList.get(position).getIsSelected()) {
                        if (mList.get(position).getName().equals("N/A") || mList.get(position).getName().equals("No") || mList.get(position).getName().equals("None")
                                || mList.get(position).getName().equals("Not Important") || mList.get(position).getName().equals("No Major Back End Logic") ||
                                mList.get(position).getName().equals("Not Required")) {
                            unSelectOthers();
                            notifyDataSetChanged();
                        }
                        else {
                            unSelectNoOption();
                            notifyDataSetChanged();
                        }
                        //mListHolder.mListImage.setImageResource(mList.get(position).getImage());
                        mListHolder.mListImage.setImageResource(mList.get(position).getSelectedImage());
                        //mListHolder.mRLGridView.setBackgroundColor(mContext.getResources().getColor(R.color.selected_grid_item));
                        if(position%2 == 0){
                            mListHolder.mRLGridView.setBackgroundResource(R.drawable.selected_three_sides_border);
                        }
                        else {
                            mListHolder.mRLGridView.setBackgroundResource(R.drawable.selected_grid_item);
                        }                        mListHolder.mListName.setTextColor(mContext.getResources().getColor(R.color.completed_text_color));
                        mList.get(position).setIsSelected(true);
                        //notifyDataSetChanged();
                        mUpdatePrice.calculatePrice();
                    }
                    else {
                        mListHolder.mListImage.setImageResource(mList.get(position).getImage());
                        mListHolder.mListName.setTextColor(mContext.getResources().getColor(R.color.white));
                        //mListHolder.mRLGridView.setBackgroundColor(mContext.getResources().getColor(R.color.unselected_grid_item));
                        if(position%2 == 0){
                            mListHolder.mRLGridView.setBackgroundResource(R.drawable.unselected_three_sides_border);
                        }
                        else {
                            mListHolder.mRLGridView.setBackgroundResource(R.drawable.unselected_grid_item);
                        }
                        mList.get(position).setIsSelected(false);
                        mUpdatePrice.calculatePrice();
                    }
                }
                public void unSelectNoOption(){
                    if (mList.get(0).getName().equals("N/A") || mList.get(0).getName().equals("No") || mList.get(0).getName().equals("None")
                            || mList.get(0).getName().equals("Not Important") || mList.get(0).getName().equals("No Major Back End Logic") ||
                            mList.get(0).getName().equals("Not Required")) {
                        mList.get(0).setIsSelected(false);
                    }
                }
            });
        }
        }
        else{
            /*mView = mListHolder.mRLGridView.getRootView();
            ViewGroup.MarginLayoutParams marginParms = new ViewGroup.MarginLayoutParams(mView.getLayoutParams());
            marginParms.setMargins(0,0,0,0);
            mView.setLayoutParams(marginParms);*/
            mListHolder.mRLGridView.setBackgroundResource(R.drawable.emptygrid_item);
            mListHolder.mListImage.setImageResource(R.drawable.empty_grid_item);
            mListHolder.mListName.setText("");
        }
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class ListHolder extends RecyclerView.ViewHolder {
        private RelativeLayout mRLGridView;
        private ImageView mListImage;
        private TextView mListName;
        private ImageView mImageSelected;

        public ListHolder(View itemView) {
            super(itemView);
            mRLGridView = (RelativeLayout) itemView.findViewById(R.id.rlGridItem);
            mListImage = (ImageView) itemView.findViewById(R.id.categoryImage);
            mListName = (TextView) itemView.findViewById(R.id.categoryName);
            mImageSelected = (ImageView) itemView.findViewById(R.id.imageSelected);
        }
    }

    public interface UpdatePriceCallback{
        public void calculatePrice();
        //public void unSelectAll
    }

    public void unSelectOthers(){
        for(int i=0;i<mList.size();i++) {
            mList.get(i).setIsSelected(false);
        }
    }
}