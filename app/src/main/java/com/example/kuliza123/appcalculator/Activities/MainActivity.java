package com.example.kuliza123.appcalculator.Activities;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Paint;
import android.graphics.Point;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;


import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kuliza123.appcalculator.Adapter.ButtonsAdapter;
import com.example.kuliza123.appcalculator.Fragments.DisplayOptionsFragment;
import com.example.kuliza123.appcalculator.Fragments.EstimateFragment;
import com.example.kuliza123.appcalculator.Models.Category;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Utils.DialogUtils;
import com.example.kuliza123.appcalculator.Utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements
        DisplayOptionsFragment.OnFragmentInteractionListener,
        ButtonsAdapter.CallBackToActivity
        {
    private int FRAGMENT_COUNTER;
    private DisplayOptionsFragment mDisplayOptionsFragment;
    private RelativeLayout mBackButtonLayout,mMainActivity,mSplashScreen;
    private TextView mCost,mTvHeader,mTvFooter;
    private HashMap<String, ArrayList<PlatformListModel>> mOptionsAppCalculatorHashMap = new HashMap<>();
    private Toolbar mToolbar;
    private FragmentTransaction mFragmentTransaction;
    private int mTotalPrice, mMultiplier,time,mTotalTime;
    private ArrayList<Category> mOverviewList;
    private GridLayoutManager mGridLayoutManager;
    private RecyclerView mButtonsView;
    private ImageView fabNext;
    private FrameLayout mMainFrame;
    LinearLayout linearLayout;
    private Button btnHome,btnHomeLeft;
    private ImageView imgHome;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createMainActivityViews();
    }
    private void createMainActivityViews() {
        generateHashMap();
        FRAGMENT_COUNTER = 0;
        imgHome = (ImageView) findViewById(R.id.btnHomeLeft);
        mMainActivity = (RelativeLayout) findViewById(R.id.rlMainActivity);
        fabNext = (ImageView) findViewById(R.id.imgNext);
        mGridLayoutManager = new GridLayoutManager(this, Constants.SLIDER_COLOMNS);
        mButtonsView = (RecyclerView) findViewById(R.id.buttonsFrame);
        imgHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        mButtonsView.setLayoutManager(mGridLayoutManager);
        mButtonsView.setAdapter(new ButtonsAdapter(this, Constants.KEY.length, FRAGMENT_COUNTER, mOptionsAppCalculatorHashMap, FRAGMENT_COUNTER, findViewById(R.id.buttonsFrame), fabNext, mButtonsView));
        mCost = (TextView) findViewById(R.id.cost);
        mMainFrame = (FrameLayout) findViewById(R.id.content_frame);
        setupToolbar(false);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ArrayListData", mOptionsAppCalculatorHashMap.get(Constants.KEY[FRAGMENT_COUNTER]));
        bundle.putInt(Constants.FRAGMENT_COUNTER, FRAGMENT_COUNTER);
        mDisplayOptionsFragment = DisplayOptionsFragment.newInstance(null, null);
        mDisplayOptionsFragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.content_frame, mDisplayOptionsFragment, Constants.KEY[FRAGMENT_COUNTER]);
        fragmentTransaction.commit();
    }

    /*  @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_estimate);
    }*/

    private void generateHashMap() {
        mOptionsAppCalculatorHashMap.put(Constants.KEY[0], createPlatformList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[1], createScreenCountList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[2], createUserMagementList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[3], createDataList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[4], createDataSourcesList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[5], createLocationList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[6], createThirdPartyCloudList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[7], createEngageUsersList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[8], createQualityList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[9], createSecurityList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[10], createAnalyticsList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[11], createReliabilityList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[12], createWebPortalList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[13], createBackEndList());
        mOptionsAppCalculatorHashMap.put(Constants.KEY[14], createOfflineList());
    }

    private ArrayList<PlatformListModel> createPlatformList() {
        ArrayList<PlatformListModel> platformList = new ArrayList<>();
        platformList.add(new PlatformListModel(R.drawable.apple, R.drawable.apple_selected, 0, "IOS", false, 0));
        platformList.add(new PlatformListModel(R.drawable.android, R.drawable.android_selected, 0, "Android", false, 0));
        platformList.add(new PlatformListModel(R.drawable.windows, R.drawable.windows_selected, 0, "Windows", false, 0));
        platformList.add(new PlatformListModel(R.drawable.blackberry, R.drawable.blackberry_selected, 0, "BlackBerry", false, 0));
        platformList.add(new PlatformListModel(R.drawable.web, R.drawable.web_selected, 0, "Web", false, 0));
        return platformList;
    }

    private ArrayList<PlatformListModel> createScreenCountList() {
        ArrayList<PlatformListModel> screenList = new ArrayList<>();
        screenList.add(new PlatformListModel(R.drawable.small,R.drawable.small_selected, 600, "Small\n(1-3 Screens)", false, 24));
        screenList.add(new PlatformListModel(R.drawable.medium, R.drawable.medium_selected,1500 , "Medium\n(3-8 Screens)", false, 60));
        screenList.add(new PlatformListModel(R.drawable.large, R.drawable.large_selected, 3600,"Large\n(9+ Screens)", false, 144));
        return screenList;
    }

    private ArrayList<PlatformListModel> createUserMagementList() {
        ArrayList<PlatformListModel> userMagementList = new ArrayList<>();
        userMagementList.add(new PlatformListModel(R.drawable.none, R.drawable.none_selected,0, "N/A", false, 0));
        userMagementList.add(new PlatformListModel(R.drawable.standard, R.drawable.standard_selected,500, "Standard", false, 20));
        userMagementList.add(new PlatformListModel(R.drawable.social, R.drawable.social_selected,400, "Social", false, 16));
        userMagementList.add(new PlatformListModel(R.drawable.enterprise,R.drawable.enterprise_selcted, 750, "Enterprise", false, 30));
        return userMagementList;
    }
    private ArrayList<PlatformListModel> createOfflineList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "Not Required", false, 0));
        dataList.add(new PlatformListModel(R.drawable.required, R.drawable.required_selected, 500, "Required", false, 20));
        return dataList;
    }

    private ArrayList<PlatformListModel> createBackEndList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none, R.drawable.none_selected,0, "No Major Back End Logic", false, 0));
        dataList.add(new PlatformListModel(R.drawable.builtwithservice, R.drawable.builtwithservice_selected, 500, "Backend Build With Ready Services", false, 20));
        dataList.add(new PlatformListModel(R.drawable.builtwithoutservice,R.drawable.builtwithoutservice_selected , 1000, "Backend Build not Service Enabled", false, 40));
        dataList.add(new PlatformListModel(R.drawable.to_create, R.drawable.to_create_selected, 2000, "Backend Needs To Be Created", false, 80));
        return dataList;
    }

    private ArrayList<PlatformListModel> createWebPortalList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "Not Important", false, 0));
        dataList.add(new PlatformListModel(R.drawable.important_web,R.drawable.important_web_selected, 1000, "Important", false, 40));
        return dataList;
    }

    private ArrayList<PlatformListModel> createReliabilityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0,  "Not Important", false, 0));
        dataList.add(new PlatformListModel(R.drawable.important,R.drawable.important_selected, 1250, "Important", false, 50));
        return dataList;
    }

    private ArrayList<PlatformListModel> createAnalyticsList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "None", false, 0));
        dataList.add(new PlatformListModel(R.drawable.minimal,R.drawable.minimal_selected, 350, "Minimal", false, 14));
        dataList.add(new PlatformListModel(R.drawable.everything,R.drawable.everything_selected, 750, "Everything", false, 30));
        return dataList;
    }

    private ArrayList<PlatformListModel> createSecurityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.complete,R.drawable.complete_selected, 800, "Complete", false, 32));
        return dataList;
    }

    private ArrayList<PlatformListModel> createQualityList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.mvp,R.drawable.mvp_selected, 500, "MVP", false, 20));
        dataList.add(new PlatformListModel(R.drawable.polished,R.drawable.polished_selected, 1250, "Polished", false, 50));
        return dataList;
    }

    private ArrayList<PlatformListModel> createEngageUsersList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.push,R.drawable.push_selected, 600, "Push, SMS, Email", false, 24));
        dataList.add(new PlatformListModel(R.drawable.advancesocialintegration,R.drawable.advancesocialintegration_selected, 600, "Advanced social \n" +"integration", false, 24));
        return dataList;
    }

    private ArrayList<PlatformListModel> createThirdPartyCloudList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "No", false,0));
        dataList.add(new PlatformListModel(R.drawable.simple_third_party_cloud,R.drawable.simple_third_party_cloud_selected ,300, "Simple", false, 12));
        dataList.add(new PlatformListModel(R.drawable.advancedthirdpartycloud,R.drawable.advancedthirdpartycloud_selected, 500, "Advanced", false, 20));
        return dataList;
    }

    private ArrayList<PlatformListModel> createLocationList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "No", false, 0));
        dataList.add(new PlatformListModel(R.drawable.simple_map,R.drawable.simple_map_selected, 400, "Simple", false, 16));
        dataList.add(new PlatformListModel(R.drawable.advancedmap,R.drawable.advancedmap_selected, 800, "Advanced", false, 32));
        return dataList;
    }

    private ArrayList<PlatformListModel> createDataSourcesList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "No", false, 0));
        dataList.add(new PlatformListModel(R.drawable.simple,R.drawable.simple_selected, 400, "Simple", false, 16));
        dataList.add(new PlatformListModel(R.drawable.advanced,R.drawable.advanced_selected, 700, "Advanced", false, 28));
        return dataList;
    }

    private ArrayList<PlatformListModel> createDataList() {
        ArrayList<PlatformListModel> dataList = new ArrayList<>();
        dataList.add(new PlatformListModel(R.drawable.none,R.drawable.none_selected, 0, "N/A", false, 0));
        dataList.add(new PlatformListModel(R.drawable.data, R.drawable.data_selected, 350, "Data", false, 14));
        dataList.add(new PlatformListModel(R.drawable.largefiles, R.drawable.largefiles_selected, 750, "Large Files", false, 30));
        return dataList;
    }

    public void setupToolbar(boolean backVisible) {
        // Set Toolbar
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView textView = (TextView)mToolbar.findViewById(R.id.toolbar_text);
        textView.setText(Constants.HEADER_STRINGS[FRAGMENT_COUNTER]);
        btnHome = (Button) mToolbar.findViewById(R.id.btnHome);
        btnHome.setVisibility(View.GONE);
        btnHomeLeft = (Button) mToolbar.findViewById(R.id.btnHomeLeft);
        btnHomeLeft.setVisibility(View.VISIBLE);;
        btnHome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        btnHomeLeft.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        setSupportActionBar(mToolbar);
    }

    @Override
    public void onBackPressed() {
        /*FRAGMENT_COUNTER--;
        FragmentManager fragmentManager = getFragmentManager();
        if(FRAGMENT_COUNTER == Constants.optionsCounter){
            LinearLayout linearLayout = (LinearLayout) findViewById(R.id.linearLayout);
            linearLayout.setVisibility(View.VISIBLE);
        }
        if (fragmentManager.getBackStackEntryCount() > 1) {
            fragmentManager.popBackStack();
        }
        else {
            finish();
        }*/
        //super.onBackPressed();
       /*finish();*/
        DialogUtils.onBackPressDialog(this, "", 0.9, 0.35, true, true);
        /*super.onBackPressed();*/
    }
    public void onYes(){
        Intent intent = new Intent(this,HomeScreen.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        //super.onBackPressed();
    }

    @Override
    public void onFragmentInteraction() {
        if(FRAGMENT_COUNTER == Constants.optionsCounter){
            Bundle bundle = new Bundle();
            bundle.putSerializable(Constants.OptionsMap, mOptionsAppCalculatorHashMap);
            bundle.putInt(Constants.TIME_KEY, mTotalTime);
            EstimateFragment estimateFragment= new EstimateFragment();
            estimateFragment.setArguments(bundle);
            Intent intent = new Intent(this,FinalActivity.class);
            intent.putExtra(Constants.finalBundleKey,bundle);
            startActivity(intent);
            return;
        }
        FRAGMENT_COUNTER++;
        setNextBackgroundColor();
        Logger.d("Fragment Counter" + FRAGMENT_COUNTER);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ArrayListData", mOptionsAppCalculatorHashMap.get(Constants.KEY[FRAGMENT_COUNTER]));
        bundle.putInt(Constants.FRAGMENT_COUNTER, FRAGMENT_COUNTER);
        mDisplayOptionsFragment = DisplayOptionsFragment.newInstance(null, null);
        mDisplayOptionsFragment.setArguments(bundle);
        mFragmentTransaction = getFragmentManager().beginTransaction();
        mFragmentTransaction.setCustomAnimations(R.anim.slide_left_small,
                R.anim.slide_right_small);
        mFragmentTransaction.replace(R.id.content_frame, mDisplayOptionsFragment, Constants.KEY[FRAGMENT_COUNTER]);
        mFragmentTransaction.commit();
    }

    @Override
    public void updateprice() {
        updatedUpdatePrice();
    }

    public void updatedUpdatePrice(){
        mTotalPrice = 0;
        mMultiplier = 0;
        mTotalTime = 0;
        ArrayList<PlatformListModel> Temp1 = new ArrayList<>();
        Temp1=mOptionsAppCalculatorHashMap.get(Constants.KEY[0]);
        for (PlatformListModel x : Temp1) {
            if (x.getIsSelected()) {
                mMultiplier += 1;
            }
        }

        if(mOptionsAppCalculatorHashMap.get(Constants.KEY[1]).get(0).getIsSelected()){
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(20);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(40);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(80);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(20);
        }
        if(mOptionsAppCalculatorHashMap.get(Constants.KEY[1]).get(1).getIsSelected()){
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(40);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(80);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(160);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(40);
        }
        if(mOptionsAppCalculatorHashMap.get(Constants.KEY[1]).get(2).getIsSelected()){
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(0).setTime(0);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(1).setTime(60);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(2).setTime(120);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[13]).get(3).setTime(240);
            mOptionsAppCalculatorHashMap.get(Constants.KEY[14]).get(1).setTime(60);
        }
        for(int i=1;i<Constants.KEY.length;i++){
            ArrayList<PlatformListModel> Temp2 = new ArrayList<>();
            Temp2=mOptionsAppCalculatorHashMap.get(Constants.KEY[i]);
            for (PlatformListModel x : Temp2) {
                if (x.getIsSelected()) {
                    mTotalTime = mTotalTime + mMultiplier*x.getTime();
                }
            }
        }
        if(mOptionsAppCalculatorHashMap.get(Constants.KEY[Constants.webPortalIndex]).get(Constants.webPortalImportant).getIsSelected() && atleatOnePlatformIsSelected(mOptionsAppCalculatorHashMap.get(Constants.KEY[Constants.platformsIndex]))) {
            mTotalTime = mTotalTime - mOptionsAppCalculatorHashMap.get(Constants.KEY[Constants.webPortalIndex]).get(Constants.webPortalImportant).getTime() * mMultiplier + mOptionsAppCalculatorHashMap.get(Constants.KEY[Constants.webPortalIndex]).get(Constants.webPortalImportant).getTime();
        }
        mTotalPrice = mTotalTime*Constants.hourlyWage;
        mCost.setText("$ " + mTotalTime * Constants.hourlyWage);
        setNextBackgroundColor();
    }

    private boolean atleatOnePlatformIsSelected(ArrayList<PlatformListModel> platformListModels) {
        for(int i=0;i<platformListModels.size();i++){
            if(platformListModels.get(i).getIsSelected()){
                return true;
            }
        }
        return false;
    }

    public int getTime() {
        int time = 0;
        for(int i=0;i<mOverviewList.size();i++){
            for(int j=0;j<mOverviewList.get(i).mList.size();j++){
                if(mOverviewList.get(i).mList.get(j).getIsSelected()) {
                    time = time + mOverviewList.get(i).mList.get(j).getTime();
                }
            }
        }
        return time;
    }

    @Override
    public void onPreviousButtonClick(int position , int fragmentCounter, int previousPosition) {
        FRAGMENT_COUNTER = fragmentCounter;
        setNextBackgroundColor();
        setNextBackgroundColor();
        createNewFragment(position, previousPosition);
    }

    private void createNewFragment(int position, int previousPosition) {
//        clearBacKStack();
        DisplayOptionsFragment displayOptionsFragment = DisplayOptionsFragment.newInstance(null,null);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("ArrayListData", mOptionsAppCalculatorHashMap.get(Constants.KEY[position]));
        bundle.putInt(Constants.FRAGMENT_COUNTER, position);
        displayOptionsFragment.setArguments(bundle);

        mFragmentTransaction = getFragmentManager().beginTransaction();
        if(previousPosition > position){
            mFragmentTransaction.setCustomAnimations(R.anim.slide_in_left,
                    R.anim.slide_in_right);
        }
        else {
            mFragmentTransaction.setCustomAnimations(R.anim.slide_left_small,
                    R.anim.slide_right_small);
        }
        mFragmentTransaction.replace(R.id.content_frame, displayOptionsFragment);
        mFragmentTransaction.commit();
    }

    @Override
    public void onNextClick() {
        /*Logger.logstatus = true;
        Logger.e("Counter : "+ getFragmentManager().getBackStackEntryCount());
        Logger.logstatus = false*/;
        onFragmentInteraction();
    }

    private void setNextBackgroundColor(){
        if(atleatOnePlatformIsSelected(mOptionsAppCalculatorHashMap.get(Constants.KEY[FRAGMENT_COUNTER]))){
            fabNext.setBackgroundColor(getResources().getColor(R.color.selected_button_color));
        }
        else{
            fabNext.setBackgroundColor(getResources().getColor(R.color.unselected_button_color));
        }
    }
}

