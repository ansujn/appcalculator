package com.example.kuliza123.appcalculator.Adapter;
import android.content.Context;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Utils.Logger;

import java.util.ArrayList;
import java.util.HashMap;
/**
 * Created by kuliza214 on 13/8/15.
 */
public class ButtonsAdapter extends RecyclerView.Adapter<ButtonsAdapter.ItemHolder> {
    private final View mSnackBarView;
    private int mButonsount,focusedItem = 0,mSelectedPosition,mFragmentCounter;
    private Context mContext;
    private CallBackToActivity mListner;
    private HashMap<String,ArrayList<PlatformListModel>> mHashMapOfOptions;
    private ImageView fabNext;
    private RecyclerView mButtonsView;

    public ButtonsAdapter(Context context,int count,int fragmentCounter,HashMap<String, ArrayList<PlatformListModel>> optionsAppCalculatorHashMap , int selectedPosition,View snackBarView,ImageView fab, RecyclerView mButtonsView){
        mButonsount = count;
        mContext = context;
        mFragmentCounter = fragmentCounter;
        mHashMapOfOptions = optionsAppCalculatorHashMap;
        mSelectedPosition = selectedPosition;
        mSnackBarView = snackBarView;
        fabNext = fab;
        this.mButtonsView = mButtonsView;
    }

    @Override
    public ButtonsAdapter.ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.button_item,parent,false);
        ButtonsAdapter.ItemHolder itemHolder = new ItemHolder(view);
        mListner = (CallBackToActivity) mContext;
        return itemHolder;
    }

    @Override
    public void onBindViewHolder(final ItemHolder holder, final int position) {
        Logger.e("MA_FC : "+mFragmentCounter+"SelectedPosition"+mSelectedPosition);
        holder.itemView.setSelected(focusedItem == position);
        if (position > Constants.ZERO_POINTER) {
            holder.btnButton.setText(String.valueOf(position + 1));
        }
        else {
            int num = position + 1;
            holder.btnButton.setText("0" + num);
        }
        if (mFragmentCounter == mSelectedPosition) {
            if (position < mFragmentCounter) {
                holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.compleated_tile));
                //holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.completed_text_color));
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.completed_text_color));
                holder.btnButton.setOnClickListener(new View.OnClickListener(){
                    @Override
                    public void onClick(View v) {
                        ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mFragmentCounter]);
                        if(atleastOneOfThemIsSelected(list)) {
                            mListner.onPreviousButtonClick(position,mFragmentCounter,mSelectedPosition);
                            mSelectedPosition = position;
                            notifyDataSetChanged();
//                            mListner.onPreviousButtonClick(position,mFragmentCounter,mSelectedPosition);
                        }
                        else{
                            mListner.onPreviousButtonClick(position, mFragmentCounter-1,mSelectedPosition);
                            mSelectedPosition = position;
                            mFragmentCounter--;
                            notifyDataSetChanged();
//                            mListner.onPreviousButtonClick(position, mFragmentCounter,mSelectedPosition);
                            //Snackbar.make(mSnackBarView,Constants.toastMessage,Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
/*
                holder.imgIndicator.setVisibility(View.GONE);
*/
            }
            if (position == mFragmentCounter) {
                holder.btnButton.setBackgroundResource(R.drawable.working_button);
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.selected_button_text_color));
                holder.btnButton.setWidth(R.dimen.selected_image_width);
                holder.btnButton.setHeight(R.dimen.selected_image_height);
                //holder.imgIndicator.setVisibility(View.VISIBLE);
            }
            else if (position > mFragmentCounter) {
                holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.un_selected_tile));
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.white));
                /*OnClickListner for buttons with positions that are greater than mFragmentCounter+1*/
                holder.btnButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Snackbar.make(mSnackBarView, Constants.snackBarOrderMessage, Snackbar.LENGTH_LONG).show();
                        Toast.makeText(mContext,Constants.snackBarOrderMessage,Toast.LENGTH_LONG).show();
                    }
                });
                if (mFragmentCounter + 1 == position) {
                    holder.btnButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            //Checking weather atleast one item is selected or not
                            ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mFragmentCounter]);
                            if (atleastOneOfThemIsSelected(list)) {
                                mFragmentCounter = mFragmentCounter + 1;
                                mSelectedPosition = mSelectedPosition +1;
                                notifyDataSetChanged();
                                mListner.onNextClick();
                            }
                            else{
                                //Snackbar.make(mSnackBarView, Constants.toastMessage, Snackbar.LENGTH_LONG).show();
                                Toast.makeText(mContext,Constants.toastMessage,Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
                //holder.imgIndicator.setVisibility(View.GONE);
            }
        }

        else{
            if(position < mFragmentCounter){
                holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.compleated_tile));
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.completed_text_color));
                holder.btnButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mSelectedPosition]);
                        if (atleastOneOfThemIsSelected(list)) {
                            mListner.onPreviousButtonClick(position ,mFragmentCounter,mSelectedPosition);
                            mSelectedPosition = position;
                            notifyDataSetChanged();
//                            mListner.onPreviousButtonClick(position ,mFragmentCounter,mSelectedPosition);
                        }
                        else{
                            //Snackbar.make(mSnackBarView, Constants.toastMessage, Snackbar.LENGTH_LONG).show();
                            Toast.makeText(mContext,Constants.toastMessage,Toast.LENGTH_LONG).show();
                        }
                    }
                });
                //holder.imgIndicator.setVisibility(View.GONE);
                if(position == mSelectedPosition){
                    holder.btnButton.setBackgroundResource(R.drawable.working_button);
                    holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.selected_button_text_color));
                    holder.btnButton.setOnClickListener(null);
                    //holder.imgIndicator.setVisibility(View.VISIBLE);
                }
            }
            else if(position == mFragmentCounter){
                holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.compleated_tile));
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.completed_text_color));
                //holder.imgIndicator.setVisibility(View.GONE);
                holder.btnButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mSelectedPosition]);
                        if(atleastOneOfThemIsSelected(list)){
                            mListner.onPreviousButtonClick(position,mFragmentCounter,mSelectedPosition);
                            mSelectedPosition = mFragmentCounter;
                            notifyDataSetChanged();
//                            mListner.onPreviousButtonClick(position,mFragmentCounter,mSelectedPosition);
                        }
                        else{
                            //Snackbar.make(mSnackBarView, Constants.toastMessage, Snackbar.LENGTH_LONG).show();
                            Toast.makeText(mContext,Constants.toastMessage,Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
            else if(position > mFragmentCounter){
                holder.btnButton.setBackgroundColor(mContext.getResources().getColor(R.color.un_selected_tile));
                //holder.imgIndicator.setVisibility(View.GONE);
                holder.btnButton.setTextColor(mContext.getResources().getColor(R.color.white));
                /*OnClickListner for buttons with positions that are greater than mFragmentCounter+1*/
                holder.btnButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //Snackbar.make(mSnackBarView, Constants.snackBarOrderMessage, Snackbar.LENGTH_LONG).show();
                        Toast.makeText(mContext,Constants.snackBarOrderMessage,Toast.LENGTH_LONG).show();
                    }
                });
                if(position == mFragmentCounter + 1){
                    holder.btnButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mSelectedPosition]);
                            if(atleastOneOfThemIsSelected(list)){
                                mFragmentCounter = mFragmentCounter + 1;
                                mSelectedPosition = mFragmentCounter;
                                notifyDataSetChanged();
                                mListner.onNextClick();
                            }
                            else{
                                //Snackbar.make(mSnackBarView, Constants.toastMessage, Snackbar.LENGTH_LONG).show();
                                Toast.makeText(mContext,Constants.toastMessage,Toast.LENGTH_LONG).show();
                            }
                        }
                    });
                }
            }
        }
        fabNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<PlatformListModel> list = mHashMapOfOptions.get(Constants.KEY[mSelectedPosition]);
                if(atleastOneOfThemIsSelected(list)){
                    if(mSelectedPosition == mFragmentCounter){
                        if(mFragmentCounter < Constants.optionsCounter) {
                            mSelectedPosition++;
                            mFragmentCounter++;
                        }
                        notifyDataSetChanged();
                        mListner.onNextClick();
                    }
                    if(mSelectedPosition < mFragmentCounter){
                        mListner.onPreviousButtonClick(mSelectedPosition+1,mFragmentCounter,mSelectedPosition);
                        mSelectedPosition++;
                        notifyDataSetChanged();
//                        mListner.onPreviousButtonClick(mSelectedPosition,mFragmentCounter,mSelectedPosition);
                    }

                    if(mSelectedPosition+3<Constants.total_questions) {
                        mButtonsView.smoothScrollToPosition(mSelectedPosition + 3);
                    }
                    else {
                        mButtonsView.smoothScrollToPosition(mSelectedPosition);
                    }
                }
                else{
                    //Snackbar.make(mSnackBarView, Constants.toastMessage, Snackbar.LENGTH_LONG).show();
                    Toast.makeText(mContext,Constants.toastMessage,Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private boolean atleastOneOfThemIsSelected(ArrayList<PlatformListModel> list) {
        for(int i=0;i<list.size();i++){
            if(list.get(i).getIsSelected()){
                return true;
            }
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return mButonsount;
    }

    public class ItemHolder extends RecyclerView.ViewHolder{
        public Button btnButton;
        /*public ImageView imgIndicator;*/
        public ItemHolder(View itemView) {
            super(itemView);
            btnButton = (Button) itemView.findViewById(R.id.btnButton);
            /*imgIndicator = (ImageView) itemView.findViewById(R.id.imgIndicator);*/
        }
    }

    public interface CallBackToActivity{
        public void onPreviousButtonClick(int position,int fragmentCounter, int previousPosition);
        public void onNextClick();
    }
}