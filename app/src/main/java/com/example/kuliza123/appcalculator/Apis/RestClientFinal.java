package com.example.kuliza123.appcalculator.Apis;

import com.example.kuliza123.appcalculator.Models.Questions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import javax.security.auth.callback.Callback;

import retrofit.RequestInterceptor;
import retrofit.RestAdapter;

/**
 * Created by kuliza214 on 9/9/15.
 */
public class RestClientFinal {
    private static final String BASE_URL = "http://52.1.91.197:8003";
    private ApiService apiService;
    public RestClientFinal(){
        Gson gson = new GsonBuilder().setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'").create();
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .setEndpoint(BASE_URL)
                .setRequestInterceptor(requestInterceptor)
                .setErrorHandler(new MyErrorHandler())
//               setConverter(new GsonConverter(gson))
                .build();
        apiService = restAdapter.create(ApiService.class);

    }
    public ApiService getApiService()
    {
        return apiService;
    }

    // Define the interceptor, add authentication headers
    RequestInterceptor requestInterceptor = new RequestInterceptor() {
        @Override
        public void intercept(RequestFacade request) {


        }
    };
}
