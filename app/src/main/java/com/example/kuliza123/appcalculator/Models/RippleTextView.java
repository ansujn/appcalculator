package com.example.kuliza123.appcalculator.Models;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Utilities;

/**
 * Created by kuliza214 on 22/9/15.
 */
public class RippleTextView extends TextView {

    public RippleTextView(Context context, String fontName) {
        super(context);
    }

    public RippleTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.setBackground(context.getResources().getDrawable(R.drawable.creame_color_ripple));
    }
}