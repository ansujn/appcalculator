package com.example.kuliza123.appcalculator.Fragments;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.ComponentCallbacks;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.provider.ContactsContract;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.kuliza123.appcalculator.Activities.MainActivity;
import com.example.kuliza123.appcalculator.Adapter.ButtonsAdapter;
import com.example.kuliza123.appcalculator.Adapter.OverViewListAdapter;
import com.example.kuliza123.appcalculator.Models.PlatformListModel;
import com.example.kuliza123.appcalculator.R;
import com.example.kuliza123.appcalculator.Utils.Constants;
import com.example.kuliza123.appcalculator.Utils.Logger;

import junit.framework.TestCase;

import org.w3c.dom.Text;
import java.security.Key;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EstimateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EstimateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EstimateFragment extends Fragment implements OverViewListAdapter.OnSubmitClick{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private HashMap<String,ArrayList<PlatformListModel>> mHashMap;
    private OnFragmentInteractionListener mListener;
    private OnButtonClickFragmentInteraction mOnButtonClickFragmentInteraction;
    private int mTotalTime;
    private TextView mTimeEstimate,mCosEstimate,mExpandList,mLearnMore,mSubmit,mEditTextEmail;
    private View view;
    private ArrayList<ImageView> mPlatformImages;
    private ArrayList<TextView> mPlatformTextViews;
    private RecyclerView mRecyclerView;
    private OnBackButtonClickFragmentInteraction mListenerBackButton;
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EstimateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EstimateFragment newInstance(String param1, String param2) {
        EstimateFragment fragment = new EstimateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    public EstimateFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setupToolbar();
        Bundle bundle = getArguments();
        mHashMap = (HashMap<String, ArrayList<PlatformListModel>>) bundle.getSerializable(Constants.OptionsMap);
       // Logger.d(mHashMap.toString());
        mTotalTime = bundle.getInt(Constants.TIME_KEY);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        TextView textView = (TextView)toolbar.findViewById(R.id.toolbar_text);
        ImageView back = (ImageView) toolbar.findViewById(R.id.back_button_left);
        Button button = (Button)toolbar.findViewById(R.id.btnHome);
        Button buttonLeft = (Button) toolbar.findViewById(R.id.btnHomeLeft);
        buttonLeft.setVisibility(View.GONE);
        button.setVisibility(View.VISIBLE);
        back.setVisibility(View.VISIBLE);
        back.setImageResource(R.drawable.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mListenerBackButton.OnBackButtonClick();
            }
        });
        textView.setText(Constants.YOUR_ESTIMATE);
        toolbar.setVisibility(View.VISIBLE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_estimate, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvOverview);
        mRecyclerView.setLayoutManager(new GridLayoutManager(getActivity(), Constants.OVERVIEW_COLOMNS));
        mRecyclerView.setAdapter(new OverViewListAdapter(getActivity(),this, mHashMap,mTotalTime,view));
        //getAllFindViewByIds();
        //mRecyclerView.setVisibility(View.GONE);
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            //mListener = (OnFragmentInteractionListener) activity;
            mOnButtonClickFragmentInteraction = (OnButtonClickFragmentInteraction) activity;
            mListenerBackButton = (OnBackButtonClickFragmentInteraction) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void getAllFindViewByIds() {
        mTimeEstimate  = (TextView) view.findViewById(R.id.tvTimeEstimate);
        mCosEstimate = (TextView) view.findViewById(R.id.tvCostEstimate);
        mExpandList = (TextView) view.findViewById(R.id.btnViewCompleateList);
        mLearnMore = (TextView) view.findViewById(R.id.tvLearnMore);
        mEditTextEmail = (EditText) view.findViewById(R.id.edtEmailId);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.rvOverview);
        mSubmit = (TextView) view.findViewById(R.id.btnSubmit);
        //mRecyclerView.setAdapter();

        mSubmit = (TextView) view.findViewById(R.id.btnSubmit);
        mPlatformImages = new ArrayList<>();
        mPlatformTextViews = new ArrayList<>();
    }

    @Override
    public void onClick(String string) {
        mOnButtonClickFragmentInteraction.onClick(string);
    }

    @Override
    public void onLearnMoreClick() {
        mOnButtonClickFragmentInteraction.onLearnMoreClick();
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnButtonClickFragmentInteraction{
        public void onClick(String string);
        public void onLearnMoreClick();
    }
    public interface OnBackButtonClickFragmentInteraction{
        public void OnBackButtonClick();
    }
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(Uri uri);
    }
}
