package com.example.kuliza123.appcalculator.Activities;

import android.content.Intent;
import android.net.Uri;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.kuliza123.appcalculator.Adapter.TabsPagerAdapter;
import com.example.kuliza123.appcalculator.Fragments.IntroScreenFive;
import com.example.kuliza123.appcalculator.Models.CirclePageIndicator;
import com.example.kuliza123.appcalculator.R;

import java.util.zip.Inflater;

public class WalkThrough extends AppCompatActivity implements IntroScreenFive.OnFragmentInteractionListener {

    private ViewPager viewPager;
    private View mFirstScreen;
    private TabsPagerAdapter mAdapter;
    private CirclePageIndicator mIndicator;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_walk_throught);
        viewPager = (ViewPager) findViewById(R.id.pager);
        mAdapter = new TabsPagerAdapter(getSupportFragmentManager());
        mIndicator = (CirclePageIndicator)findViewById(R.id.indicator);
        viewPager.setAdapter(mAdapter);
        mIndicator.setViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_walk_throught, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {
        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
