package com.example.kuliza123.appcalculator.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.example.kuliza123.appcalculator.Fragments.IntroScreenFive;
import com.example.kuliza123.appcalculator.Fragments.IntroScreenFour;
import com.example.kuliza123.appcalculator.Fragments.IntroScreenOne;
import com.example.kuliza123.appcalculator.Fragments.IntroScreenThree;
import com.example.kuliza123.appcalculator.Fragments.IntroScreenTwo;

public class TabsPagerAdapter extends android.support.v4.app.FragmentPagerAdapter{
    public TabsPagerAdapter(FragmentManager fm){
        super(fm);
    }
    @Override
    public Fragment getItem(int position) {
        if(position == 0){
            return new IntroScreenOne();
        }
        if(position == 1){
            return new IntroScreenTwo();
        }
        if(position == 2){
            return new IntroScreenThree();
        }
        if(position == 3){
            return new IntroScreenFour();
        }
        if(position == 4){
            return new IntroScreenFive();
        }
        return null;
    }
    @Override
    public int getCount() {
        return 5;
    }
}