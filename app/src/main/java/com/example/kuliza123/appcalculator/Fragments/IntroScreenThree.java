package com.example.kuliza123.appcalculator.Fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.kuliza123.appcalculator.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class IntroScreenThree extends Fragment {


    public IntroScreenThree() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_intro_screen_three, container, false);
    }


}
