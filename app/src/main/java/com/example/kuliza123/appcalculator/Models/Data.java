package com.example.kuliza123.appcalculator.Models;

public class Data {


    private String q0;
    private String q1;
    private String q2;
    private String q3;
    private String q4;
    private String q5;
    private String q6;
    private String q7;
    private String q8;
    private String q9;
    private String q10;
    private String q11;
    private String q12;
    private String q13;
    private String q14;

    /**
     *
     * @return
     * The q0
     */
    public String getQ0() {
        return q0;
    }

    /**
     *
     * @param q0
     * The q0
     */
    public void setQ0(String q0) {
        this.q0 = q0;
    }

    /**
     *
     * @return
     * The q1
     */
    public String getQ1() {
        return q1;
    }

    /**
     *
     * @param q1
     * The q1
     */
    public void setQ1(String q1) {
        this.q1 = q1;
    }

    /**
     *
     * @return
     * The q2
     */
    public String getQ2() {
        return q2;
    }

    /**
     *
     * @param q2
     * The q2
     */
    public void setQ2(String q2) {
        this.q2 = q2;
    }

    /**
     *
     * @return
     * The q3
     */
    public String getQ3() {
        return q3;
    }

    /**
     *
     * @param q3
     * The q3
     */
    public void setQ3(String q3) {
        this.q3 = q3;
    }

    /**
     *
     * @return
     * The q4
     */
    public String getQ4() {
        return q4;
    }

    /**
     *
     * @param q4
     * The q4
     */
    public void setQ4(String q4) {
        this.q4 = q4;
    }

    /**
     *
     * @return
     * The q5
     */
    public String getQ5() {
        return q5;
    }

    /**
     *
     * @param q5
     * The q5
     */
    public void setQ5(String q5) {
        this.q5 = q5;
    }

    /**
     *
     * @return
     * The q6
     */
    public String getQ6() {
        return q6;
    }

    /**
     *
     * @param q6
     * The q6
     */
    public void setQ6(String q6) {
        this.q6 = q6;
    }

    /**
     *
     * @return
     * The q7
     */
    public String getQ7() {
        return q7;
    }

    /**
     *
     * @param q7
     * The q7
     */
    public void setQ7(String q7) {
        this.q7 = q7;
    }

    /**
     *
     * @return
     * The q8
     */
    public String getQ8() {
        return q8;
    }

    /**
     *
     * @param q8
     * The q8
     */
    public void setQ8(String q8) {
        this.q8 = q8;
    }

    /**
     *
     * @return
     * The q9
     */
    public String getQ9() {
        return q9;
    }

    /**
     *
     * @param q9
     * The q9
     */
    public void setQ9(String q9) {
        this.q9 = q9;
    }

    /**
     *
     * @return
     * The q10
     */
    public String getQ10() {
        return q10;
    }

    /**
     *
     * @param q10
     * The q10
     */
    public void setQ10(String q10) {
        this.q10 = q10;
    }

    /**
     *
     * @return
     * The q11
     */
    public String getQ11() {
        return q11;
    }

    /**
     *
     * @param q11
     * The q11
     */
    public void setQ11(String q11) {
        this.q11 = q11;
    }

    /**
     *
     * @return
     * The q12
     */
    public String getQ12() {
        return q12;
    }

    /**
     *
     * @param q12
     * The q12
     */
    public void setQ12(String q12) {
        this.q12 = q12;
    }

    /**
     *
     * @return
     * The q13
     */
    public String getQ13() {
        return q13;
    }

    /**
     *
     * @param q13
     * The q13
     */
    public void setQ13(String q13) {
        this.q13 = q13;
    }

    /**
     *
     * @return
     * The q14
     */
    public String getQ14() {
        return q14;
    }

    /**
     *
     * @param q14
     * The q14
     */
    public void setQ14(String q14) {
        this.q14 = q14;
    }

}